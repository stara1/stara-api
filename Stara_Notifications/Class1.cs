﻿using System;
using System.Collections.Generic;
using System.Net;
using RestSharp;

namespace Stara_Notifications
{
    public class Class1
    {

        private readonly RestClient restClient;

        public Class1(RestClient restClient)
        {
            this.restClient = restClient;
        }

        public IRestResponse CallApi(String path, RestSharp.Method method, Object postBody, Dictionary<String, String> formParams, Dictionary<String, FileParameter> fileParams)
        {
            var request = new RestRequest(path, method);

            if(postBody != null)
                request.AddJsonBody(postBody);

            // add form parameter, if any
            if (formParams != null)
            {
                foreach (var param in formParams)
                    request.AddParameter(param.Key, param.Value, ParameterType.GetOrPost);
            }


            // add file parameter, if any
            if (fileParams != null)
            {
                foreach (var param in fileParams)
                    request.AddFile(param.Value.Name, param.Value.FileName, param.Value.ContentType);
            }

            //serve para ignorar a validacao SSL, não estava conectando com a api
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

            try
            {
                
                IRestResponse response = restClient.Post(request);
                return response;
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }
    }
}
