using RestSharp;
using Stara_Notifications;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using Xunit;


namespace XUnitTestProject1
{
    public class UnitTest1
    {
        //message webex sem file
        [Fact]
        public void WebexText()
        {
            RestClient restClient = new RestClient();
            restClient.BaseUrl = new Uri("https://localhost:5001/api");
            Class1 instance = new Class1(restClient);

            var body = new
            {
                email = "jessen@stara.com.br",
                texto = "funciono"
            };

            var response = instance.CallApi("/messageWebexWithoutFile", Method.POST, body, null, null);

            Assert.Equal("OK", response.StatusCode.ToString());
        }

        //menssagem email sem anexo
        [Fact]
        public void EmailText()
        {
            RestClient restClient = new RestClient();
            restClient.BaseUrl = new Uri("https://localhost:5001/api");
            Class1 instance = new Class1(restClient);

            var body = new
            {
                email = "jessen.dellibrasil@gmail.com",
                assunto = "testando client",
                texto = "funciono"
            };

            var response = instance.CallApi("/emailWithoutFile", Method.POST, body, null, null);

            Assert.Equal("OK", response.StatusCode.ToString());
        }

        //menssagem webex com anexo
        [Fact]
        public void WebexFile()
        {
            RestClient restClient = new RestClient();
            restClient.BaseUrl = new Uri("https://localhost:5001/api");
            Class1 instance = new Class1(restClient);

            Dictionary<string, string> form = new Dictionary<string, string>();
            Dictionary<string, FileParameter> file = new Dictionary<string, FileParameter>();

            form.Add("email", "jessen@stara.com.br");
            form.Add("texto", "testando webex file");


            var fileBytes = File.ReadAllBytes(@"C:\Users\rfpan\source\repos\XUnitTestProject1\TCC.pdf");

            file.Add("files", FileParameter.Create("files", fileBytes, "TCC.pdf"));

            var response = instance.CallApi("/sendMessageWebex", Method.POST, null, form, file);

            Assert.Equal("OK", response.StatusCode.ToString());
        }

        //menssagem email com anexo
        [Fact]
        public void EmailFile()
        { 
            RestClient restClient = new RestClient();
            restClient.BaseUrl = new Uri("https://localhost:5001/api");
            Class1 instance = new Class1(restClient);

            Dictionary<string, string> form = new Dictionary<string, string>();
            Dictionary<string, FileParameter> file = new Dictionary<string, FileParameter>();

            form.Add("email", "jessen@stara.com.br");
            form.Add("texto", "testando email file");
            form.Add("assunto", "alooo");

            var fileBytes = File.ReadAllBytes(@"C:\Users\rfpan\source\repos\XUnitTestProject1\TCC.pdf");

            file.Add("uploadedFile", FileParameter.Create("uploadedFile", fileBytes, "TCC.pdf"));

            var response = instance.CallApi("/sendEmail", Method.POST, null, form, file);

            Assert.Equal("OK", response.StatusCode.ToString());
        }
    }
}
